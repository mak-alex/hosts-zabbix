local HOSTS_HOME="/usr/local/bin/hosts-zabbix";
local lscript_path=HOSTS_HOME.."/lua"
local JSONf = assert(loadfile(lscript_path.."/JSON.lua"));
package.path = package.path..";/usr/lib64/lua/luarocks/share/lua/5.1/?.lua;/usr/lib64/lua/luarocks/share/lua/5.1/?/?.lua;./lua/?.lua;./?.lua;"..lscript_path.."/?.lua;";
local JSON,HOSTS,CONF=JSONf(),{}
local zapi=require("zapi");

HOSTS={
  ["ABOUTME"]={
    ["_MODULE"]         = "Hosts-Zabbix",
    ["_VERSION"]        = "Hosts-Zabbix v0.1.0-rc1",
    ["_AUTHOR"]         = "Alexandr Mikhailenko (he also) FlashHacker",
    ["_URL"]            = "https://bitbucket.org/enlab/Hosts-Zabbix",
    ["_MAIL"]           = "flashhacker1988@gmail.com",
    ["_COPYRIGHT"]      = "Design Bureau of Industrial Communications LTD, 2015. All rights reserved.",
    ["_LICENSE"]        = [[
      MIT LICENSE
      Copyright (c) 2015 Mikhailenko Alexandr Konstantinovich (a.k.a) Alex M.A.K
      Permission is hereby granted, free of charge, to any person obtaining a
      copy of this software and associated documentation files (the
      "Software"), to deal in the Software without restriction, including
      without limitation the rights to use, copy, modify, merge, publish,
      distribute, sublicense, and/or sell copies of the Software, and to
      permit persons to whom the Software is furnished to do so, subject to
      the following conditions:
      The above copyright notice and this permission notice shall be included
      in all copies or substantial portions of the Software.
      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
      OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
      MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
      IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
      CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
      TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ]],
    ["_DESCRIPTION"]    = "Get host information..."
  },
	--- identity function and start parse
	-- @param name - CONF["this_name"]["delimiter"]
	-- @usage HOSTS["INDENT"]("zabbix")
	-- @return state "1" or "0" and loggind data to file
	["INDENT"]=function(name)
		--if user~=nil and user~='' and password~=nil and password~='' and url~=nil and url~='' then
		if CONF[name]["url"]~='' and CONF[name]["user"]~='' and CONF[name]["password"]~='' then
			print("\nStart identification settings")
			if zapi then
				print(string.format("Connect to server %s", CONF[name]["url"]:gsub("zabbix/api_jsonrpc.php","")))
				if tonumber(CONF[name]["debug"])==1 then zapi.tracetoggle() end
      	zapi.login(CONF[name]["url"],CONF[name]["user"],CONF[name]["password"]);
      end	
			return 0
		else
			for k,v in pairs({user,password,url}) do
      	if k==1 then errArg="user" elseif k==2 then errArg="password" else errArg="url" end
      	if v==nil or v=='' then print(string.format("is not filled one of the input parameters:: %s",errArg)) end
			end
			return 1
		end
	end,

	--- 
	-- @usage HOSTS["PARSE"]()
	-- @param name - CONF["this_name"]["delimiter"]
	-- @return key:value;;...:...;
	["PARSE"]=function(name)
		local setTypeFilter,setMacroFilter
		local except={}
		local result,attributes={},{}
		print("Get hosts information\n")
		local response=zapi.call("host.get",{
      ["expandData"]=1,
      ["expandComment"]=1,
      ["expandExpression"]=1,
      ["selectInterfaces"]="extend",
      ["selectMacros"]="extend"
    })
    local hosts = JSON:decode(response); 
    local back2JSON = JSON:encode_pretty(hosts);
    zapi.trace(back2JSON);
    for i=1,#(hosts.result) do
    	for k,v in pairs(hosts.result[i]) do
    		if type(v)=="table" then
	    		table.foreach(v,function(kk,vv)
	    			result[i]={}
		    		if vv["macro"]=="{$SNMP_COMMUNITY}" then
			    		table.foreach(vv, function(key,value)
			    			if HOSTS["ISIN"](tostring(key),{"macro","value"}) then
			    				if value==nil or value=='' then value=0 end
			    				attributes[key]=value
			    			end
			    		end)
			    	end
			    	if vv["type"]~=nil and tonumber(vv["type"])==2 then
			    		if hosts.result[i]["host"]~=nil and hosts.result[i]["name"]~=nil then
		    				attributes["host"]=hosts.result[i]["host"]
		    				attributes["name"]=hosts.result[i]["name"]
			    		end
			    		table.foreach(vv, function(key,value)
			    			if HOSTS["ISIN"](tostring(key),{"type", "port", "ip"}) then
			    				if value==nil or value=='' then value=0 end
			    				attributes[key]=value
			    			end
			    		end)
			    		if attributes["host"]~=nil and attributes["name"]~=nil 
			    		and attributes["macro"]~=nil and attributes["value"]~=nil 
			    		and attributes["type"]~=nil then
			    			print(HOSTS["toCSV"](attributes,CONF[name]["delimiter"]))
			    		end
			    	end
	    		end)
	    	end
    	end
    end
	end,

	--- Used to escape "'s by toCSV
	-- @param s - array elements
	-- @usage for k,v in pairs(table) do HOSTS[escape](v) end
	-- @return delimited string
	["escapeCSV"]=function(s)
	  if string.find(s, '[,"]') then
	    s = '"' .. string.gsub(s, '"', '""') .. '"'
	  end
	  return s
	end,

	-- Convert from table to CSV string
	-- @param tt - Lua table
	-- @usage delimiter - ";" or maybe other delimiter
	-- @return string - with no delimiters
	["toCSV"]=function(tt,delimiter)
	  local s = ""
		-- assumption is that fromCSV and toCSV maintain data as ordered array
	  for k,v in pairs(tt) do
	  	if HOSTS["ISIN"](k,{"host","name","macro","type","value","port","ip"}) then
	    	s = s .. delimiter .. HOSTS["escapeCSV"](v)
	    end
	  end
	  return string.sub(s, 2)      -- remove first comma
	end,

	["ISIN"]=function(str,array)
	  local theResult=false
	  for i,v in ipairs(array) do 
	  	if(v == str) then 
	  		theResult=true 
	  	end 
	  end
	  return theResult
	end,

	["CUT"]=function(value)
    return value:gsub("[{]",""):gsub("[}]",""):gsub("[$]","");
  end,

  --- The function mcc.of a general purpose data recording file
  -- @param location -- path to save file
  -- @param filename -- name file to save
  -- @param data     -- any string value
  -- @usage String2File(location, filename, "Welcome\nBla-bla-bla")
  -- @usage String2File(location, filename, unpack(array))
  -- @return state "1" or "0" and loggind data to file
  ["String2File"]=function(location, filename, data)
    if location and filename and data ~= nil then
      local path = (location or lscript_path) .. "/" .. filename -- определяем путь и имя файла
      file = assert(io.open(path, "a"))                   -- открываем файл в режиме записи
      io.output(file)                                     -- указываем режим вход или выход
      io.write(data, "\n")                                -- записываем наши данные в файл
      io.close(file)                                      -- закрываем файл чтобы не висел в памяти
      return 0
    else
      print( "variable location and filename/data are empty or not correctly filled in")
      return 1
    end
  end,

  --- The function mcc.of a general purpose recording file in of string
  -- @param location -- path to save file
  -- @param filename -- name file to save
  -- @usage fileToString(location, filename)
  -- @usage fileToString("/home/", "filename.json")
  -- @return json_string or 1
  ["File2String"]=function(location, filename)
    if location~=nil and filename ~= nil then
      local path = (location or HOSTS_HOME) .. "/" .. filename
      local jsonConfigFile = assert(io.open(path, "r"))
      local stringFile = jsonConfigFile:read "*a"
      jsonConfigFile:close()
      return stringFile
    else
      print(string.format("I can not convert the file: %s into a string", filename))
      return 1
    end
  end, 
}

function main()
	CONF = JSON:decode(HOSTS["File2String"]("/usr/etc/","zbx-hosts-2-snmpd-proxy-conf.json"))
	for k,v in pairs(CONF) do if HOSTS["INDENT"](k) then HOSTS["PARSE"](k) end end
end

return main()